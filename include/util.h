#ifndef UTIL_H_
#define UTIL_H_
#define BUFFER_LEN 255
#define NELEMS(x) (sizeof (x) / sizeof (x[0]))
#define START_SIZE_ARRAY 8

typedef enum {START, END} part;

typedef struct {
    size_t length;
    size_t max;
    char *items[];
} sarray;

void error(const char *msg);
FILE *open_file(const char *filename, const char *mode);
void close_file(FILE *f);
sarray *create_sarray_from_file(FILE *filename, sarray **exclude);
void get_host(char *line);
void loop_over_sarray(sarray *arr);
int binary_search(sarray *arr, const char *str);
void free_sarray(sarray *arr);
void sort_sarray(sarray *arr);
int compare_sarrays(sarray *one, sarray *two);
int compare_str(const void *one, const void *two);
void print_sarray(sarray *arr);
void put_item_in_sorted_sarray(sarray **arr, const char *item, int index);
void check_space_sarray(sarray **arr);
void append_item_to_sarray(sarray **arr, const char *item);
void move_items_in_sarray(sarray **arr, int index);
void merge_sarrays(sarray **current, sarray **extra, sarray **whitelist);
sarray *read_hosts(FILE *f);
void write_sarray_to_file(FILE *f, sarray *arr, const char *prefix);
int check_access(const char *path, int how, int throw_error);
void copy_file(const char *infile, const char *outfile);
char *append_suffix_to_str(const char *str, const char *suffix);
int is_valid_prefix(const char *prefix);
int str_matches(regex_t *comp, const char *str);
void init_regex(const char *regex, regex_t *comp);
int get_connection();
int read_into_file(int socket, FILE *f);
int starts_with(const char *str, const char *pre);
void get_part_of_url(char *url, part startOrEnd);
void move_char_array(char *str, unsigned int places);
int say_to_server(int socket, const char *msg);
int put_header(FILE *f);
sarray *create_sarray_from_path(const char *path, int must_exist, sarray **whitelist);
int backup_file(const char *path, const char *ext);
void print_sarray(sarray *arr);

#endif  /* UTIL_H_ */
