# NoYuck

Ad and site blocker that uses the hosts file on your computer

NoYuck works by addings domains to your hosts file and pointing the IP address to your localhost 127.0.0.1 or 0.0.0.0, thereby making it impossible to resolve the URL as it normally would. Thus the content that would be normally fetched is blocked, and your computer is spared a lot of yuck...

## How to use NoYuck

### Using local hosts files

    ./noyuck --current my_current_host_file --new other_hosts_file

This will merge `current_host_file` with `other_hosts_file` and create an
output file named `merged_hosts` in the current directory.

If you would like the current hosts file to be replaced in place, you can
specify the option `--inplace` (or `-i` for short). A backup will be created
of the current hosts file.

### Using online hosts files

    ./noyuck --current my_current_host_file --file file_with_URLs

This will query all valid URLs in the `file_with_URLs`, download their content
and merge it with the contents `my_current_host_file`. Unless `--inplace` (or
`-i` for short) has been specified, a new file `merged_hosts` will be created.

### Whitelist

    ./noyuck --current curr --new update --whitelist white.list

You can specifify a hosts file that contains domains that should _not_ be
blocked. This file should just be a regular hosts file:

    0.0.0.0	ilovekittens.com

### Specify IP to which all traffic should be routed

By default, all traffic will be routed to `0.0.0.0`. If you want to change
this, you can use the `--prefix` (or `-p`) switch and specify another IP.
