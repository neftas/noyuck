CC=gcc
CFLAGS=-Wall -Wextra -O0 -ggdb3
LDFLAGS=
LDTESTFLAGS=-lcmocka
EXTRA=${extra}

noyuck: noyuck.o util.o include/util.h
	$(CC) $(CFLAGS) $(EXTRA) -I. -o noyuck noyuck.o util.o

testutil: tests/testutil.o util.o
	$(CC) $(CFLAGS) $(LDTESTFLAGS) -I. -o tests/testutil tests/testutil.o util.o

long_args_test: long_args_test.c
	$(CC) $(CFLAGS) -o long_args_test long_args_test.c

copytest: copytest.o util.o include/util.h
	$(CC) $(CFLAGS) -I. -o copytest copytest.o util.o

%.o: %.c
	$(CC) $(CFLAGS) $(EXTRA) -I. -c -o $@ $<

.PHONY: clean

clean:
	find . -type f -iname "*.o" -delete
	rm -v vgcore* noyuck output tests/testutil long_args_test
