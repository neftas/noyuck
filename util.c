#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <regex.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <netdb.h>
#include <fcntl.h>
#include <stropts.h>
#include <time.h>
#include "include/util.h"

void error(const char *msg) {
    fprintf(stderr, "%s: %s\n", msg, strerror(errno));
    exit(EXIT_FAILURE);
}

FILE *open_file(const char *filename, const char *mode) {
    FILE *f = NULL;
    if ((f = fopen(filename, mode)) == NULL) {
        error("Could not open file");
    }
    return f;
}

void close_file(FILE *f) {
    if (fclose(f)) {
        error("Could not properly close file");
    }
}

sarray *create_sarray_from_file(FILE *filename, sarray **exclude) {

    char buffer[BUFFER_LEN];
    /* initialize array that holds the merged hosts */
    sarray *all_hosts = malloc(sizeof (sarray) + (START_SIZE_ARRAY * sizeof (char*)));
    all_hosts->length = 0;
    all_hosts->max = START_SIZE_ARRAY;

    while (fgets(buffer, BUFFER_LEN, filename) != NULL) {
        buffer[strcspn(buffer, "\r\n")] = '\0';
        get_host(buffer);
        if (strlen(buffer) > 0 && !starts_with(buffer, "localhost")) {
            if (exclude != NULL && binary_search((*exclude), buffer) >= 0) {
                continue;
            }
            append_item_to_sarray(&all_hosts, buffer);
        }
    }

    return all_hosts;
}

void get_host(char *line) {
    char temp[BUFFER_LEN];
    char format[BUFFER_LEN];
    snprintf(format, BUFFER_LEN, "%s%d%s", "%*i.%*i.%*i.%*i %",
            BUFFER_LEN, "[^ ]s%*s");
    // printf("Format looks like: %s\n", format);
    if (sscanf(line, format, temp) > 0) {
        strncpy(line, temp, BUFFER_LEN);
        line[strlen(temp)] = '\0';
    } else {
        line[0] = '\0';
    }
}

void loop_over_sarray(sarray *arr) {
    for (int i = 0, j = arr->length; i < j; i++) {
        printf("Next: %s\n", arr->items[i]);
    }
}

sarray *read_hosts(FILE *f) {

    char buffer[BUFFER_LEN];
    /* initialize array that holds the merged hosts */
    long len_all_hosts = START_SIZE_ARRAY;
    sarray *all_hosts = malloc(sizeof (sarray) + (len_all_hosts * sizeof (char*)));
    all_hosts->length = 0;
    all_hosts->max = len_all_hosts;

    while (fgets(buffer, BUFFER_LEN, f) != NULL) {
        if (all_hosts->length > 0 && all_hosts->length % START_SIZE_ARRAY == 0) {
            /* make array larger */
            all_hosts->max *= 2;
            all_hosts = realloc(all_hosts, sizeof (sarray) + (all_hosts->max * sizeof (char *)));
        }

        buffer[strcspn(buffer, "\r\n")] = '\0';
        get_host(buffer);
        if (strlen(buffer) > 0) {
            all_hosts->items[all_hosts->length] = strndup(buffer, BUFFER_LEN);
            all_hosts->length++;
        }
    }
    return all_hosts;
}

int binary_search(sarray *arr, const char *str) {
    int low = 0;
    int high = arr->length - 1;
    int middle;
    while (high >= low) {
        middle = (high + low) / 2;
        int result = strncmp(str, arr->items[middle], BUFFER_LEN);
        if (result < 0) {
            high = middle - 1;
        } else if (result > 0) {
            low = middle + 1;
        } else {
            return middle;
        }
    }
    if (low > high) {
        return -(low + 1);
    } else {
        return -(middle + 1);
    }
}

int compare_str(const void *one, const void *two) {
    char *first = * (char **) one;
    char *second = * (char **) two;
    return strncmp(first, second, BUFFER_LEN);
}

int compare_sarrays(sarray *one, sarray *two) {
    size_t len_one = one->length;
    size_t len_two = two->length;
    if (len_one != len_two) {
        return 1;
    }
    for (unsigned int i = 0; i < len_one; i++) {
        if (strncmp(one->items[i], two->items[i], BUFFER_LEN) != 0) {
            return 1;
        }
    }
    return 0;
}

void sort_sarray(sarray *arr) {
    qsort(arr->items, arr->length, sizeof (char *), compare_str);
}

void free_sarray(sarray *arr) {
    for (int i = 0, j = arr->length; i < j; i++) {
        free(arr->items[i]);
    }
    free(arr);
}

void print_sarray(sarray *arr) {
    if (arr == NULL) {
        return;
    }
    printf("{");
    for (unsigned int i = 0; i < arr->length; i++) {
        printf("%s, ", arr->items[i]);
    }
    printf("}\n");
}

void check_space_sarray(sarray **arr) {
    if ((*arr)->length == (*arr)->max) {
        (*arr)->max *= 2;
        sarray *temp;
        temp = realloc(*arr, sizeof (sarray) + ((*arr)->max * sizeof (char *)));
        if (temp == NULL) {
            error("Could not reallocate space for sarray");
        } else {
            *arr = temp;
        }
    }
}

void append_item_to_sarray(sarray **arr, const char *item) {
    check_space_sarray(arr);
    (*arr)->items[(*arr)->length] = strndup(item, BUFFER_LEN);
    (*arr)->length++;
}

void move_items_in_sarray(sarray **arr, int index) {
    for (int i = (*arr)->length - 1; i >= index; i--) {
        (*arr)->items[i+1] = (*arr)->items[i];
    }
}

void put_item_in_sorted_sarray(sarray **arr, const char *item, int index) {
    check_space_sarray(arr);
    /* move every item one index to the right */
    move_items_in_sarray(arr, index);
    /* account for the item that was inserted */
    (*arr)->length++;
    /* put item at index */
    (*arr)->items[index] = strndup(item, BUFFER_LEN);
}

void merge_sarrays(sarray **current, sarray **extra, sarray **whitelist) {
    for (unsigned int i = 0; i < (*extra)->length; i++) {
        char *next_str = (*extra)->items[i];
        if (whitelist != NULL && binary_search((*whitelist), next_str) >= 0) {
            puts("got one");
            continue;
        }
        int index;
        if ((index = binary_search((*current), next_str)) < 0) {
            /* normalize index */
            index = -(index + 1);
            put_item_in_sorted_sarray(current, next_str, index);
        }
    }
}

void write_sarray_to_file(FILE *f, sarray *arr, const char *prefix) {
    for (int i = 0; i < arr->length; i++) {
        fprintf(f, "%s\t%s\n", prefix, arr->items[i]);
    }
}

int check_access(const char *path, int how, int throw_error) {
    if (access(path, how) == -1) {
        /* we just want to see if the file exists */
        if (!throw_error && errno == ENOENT) {
            return 0;
        }
        char buffer[BUFFER_LEN];
        snprintf(buffer, BUFFER_LEN, "Path '%s' cannot be used", path);
        error(buffer);
    }
    return 1;
}

void copy_file(const char *infile, const char *outfile) {
    /* open file for reading */
    FILE *read = open_file(infile, "r");

    /* open file for writing */
    FILE *write = open_file(outfile, "w");

    char buffer[BUFFER_LEN];
    while (fgets(buffer, BUFFER_LEN, read) != NULL) {
        fputs(buffer, write);
    }

    close_file(read);
    close_file(write);
}

char *append_suffix_to_str(const char *str, const char *suffix) {
    size_t len_suffix = strlen(suffix);
    len_suffix = (len_suffix >= BUFFER_LEN) ? (BUFFER_LEN - 1) : len_suffix;
    char *result = malloc(BUFFER_LEN);  /* sizeof (char) == 1 */
    result[0] = '\0';
    /* take into account that strncat writes n+1 bytes to dest */
    strncat(result, str, BUFFER_LEN - (len_suffix + 1));
    strncat(result, suffix, len_suffix);
    return result;
}

int is_valid_prefix(const char *prefix) {
    int parts[4] = {0, 0, 0, 0};
    const char *format = "%3i.%3i.%3i.%3i";
    if (sscanf(prefix, format, &parts[0], &parts[1], &parts[2], &parts[3]) == 4) {
        for (int i = 0, j = NELEMS(parts); i < j; i++) {
            if (parts[i] < 0 || parts[i] > 255) {
                return 0;
            }
        }
        return 1;
    }
    return 0;
}

int str_matches(regex_t *comp, const char *str) {
    char errbuf[BUFFER_LEN];
    int ret;
    ret = regexec(comp, str, 0, NULL, 0);
    if (!ret) {
        puts("String matches!");
        return 1;
    } else if (ret == REG_NOMATCH) {
        puts("String does NOT match");
        return 0;
    } else {
        regerror(ret, comp, errbuf, sizeof (errbuf));
        fprintf(stderr, "Regex match failed: %s\n", errbuf);
        exit(EXIT_FAILURE);
    }
}

void init_regex(const char *regex, regex_t *comp) {
    int ret_regex;
    ret_regex = regcomp(comp, regex, REG_EXTENDED);
    if (ret_regex) {
        error("Could not compile regex");
    }
}

int get_connection(const char *url, const char *port) {
    int ret, s;
    struct addrinfo *res;
    struct addrinfo hints;
    memset(&hints, 0, sizeof (hints));
    hints.ai_family = PF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    ret = getaddrinfo(url, port, &hints, &res);
    if (ret != 0) {
        error("Error looking up address info");
    }

    s = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
    if (s == -1) {
        close(s);
        error("Error creating socket");
    }

    /* set a timeout for the socket */
    struct timeval tv;
    tv.tv_sec = 4;  /* 4 secs timeout */
    tv.tv_usec = 0;  /* Not init'ing this can cause strange errors */

    ret = setsockopt(s, SOL_SOCKET, SO_RCVTIMEO, (char *) &tv, sizeof(struct timeval));
    if (ret == -1) {
        error("Cannot set socket timeout");
    }

    ret = connect(s, res->ai_addr, res->ai_addrlen);
    freeaddrinfo(res);
    if (ret == -1) {
        close(s);
        error("Could not connect to remote host");
    }

    return s;
}

int say_to_server(int socket, const char *msg) {
    int result = send(socket, msg, strlen(msg), 0);
    if (result == -1) {
        error("Error talking to server");
    }
    return result;
}

int read_into_file(int socket, FILE *f) {
    size_t recv_len = BUFFER_LEN * 4;
    char rec[recv_len];
    ssize_t bytesReceived = recv(socket, rec, recv_len, 0);
    while (bytesReceived) {
        if (bytesReceived == -1) {
            if (errno != EWOULDBLOCK && errno != EAGAIN) {
                error("Can't read from server");
            } else {
                return 1;
            }
        }
        rec[bytesReceived] = '\0';
        fprintf(f, "%s", rec);
        bytesReceived = recv(socket, rec, recv_len, 0);
    }
    return 1;
}

int starts_with(const char *str, const char *pre) {
    size_t len_str = strlen(str);
    size_t len_pre = strlen(pre);
    if (len_pre > len_str) {
        return 0;
    }
    return (strncmp(str, pre, len_pre) == 0) ? 1 : 0;
}

void move_char_array(char *str, unsigned int places) {
    size_t len = strlen(str);
    for (int i = 0; i < len; i++) {
        if (i + places >= len) {
            str[i] = '\0';
            break;
        } else {
            str[i] = str[i + places];
        }
    }
}

void get_part_of_url(char *url, part startOrEnd) {
    size_t offset = 0;
    const char *http = "http://";
    const char *https = "https://";
    if (starts_with(url, http)) {
        offset = strlen(http);
    } else if (starts_with(url, https)) {
        offset = strlen(https);
    }
    size_t first_dash = strcspn(url + offset, "/") + offset;
    if (startOrEnd == START) {
        url[first_dash] = '\0';
        if (offset != 0) {
            move_char_array(url, offset);
        }
    } else if (startOrEnd == END) {
        move_char_array(url, first_dash);
    }
}

/* Return the number of bytes printed to file. */
int put_header(FILE *f) {
    time_t t;
    time(&t);
    char *current_time = ctime(&t);
    char header[BUFFER_LEN];
    snprintf(header, BUFFER_LEN,
        "# Hosts file generated by NoYuck on %s\n"
        "127.0.0.1\tlocalhost.localdomain\tlocalhost\n"
        "::1\t\tlocalhost.localdomain\tlocalhost\n"
        "\n# Start of fetched hosts\n\n",
        current_time);
    return fprintf(f, header);
}

sarray *create_sarray_from_path(const char *path,
                                int must_exist,
                                sarray **whitelist)
{
    FILE *f;
    sarray *hosts = NULL;
    if (path != NULL) {
        check_access(path, R_OK, must_exist);
        f = open_file(path, "r");
        hosts = create_sarray_from_file(f, whitelist);
        sort_sarray(hosts);
        close_file(f);
    }
    return hosts;
}

int backup_file(const char *path, const char *ext) {
    char *path_backup = append_suffix_to_str(path, ext);
    copy_file(path, path_backup);
    free(path_backup);
}
