#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <unistd.h>
#include <stddef.h>
#include <regex.h>
#include <setjmp.h>
#include <cmocka.h>
#include "include/util.h"

static void null_test_should_succeed(void **state) {
    (void) state;
}

static void get_host_with_empty_string_should_return_empty_string(void **state) {
    char str[BUFFER_LEN] = "";
    char *expected = "";
    get_host(str);
    assert_string_equal(str, expected);
}

static void get_host_with_normal_input_should_return_host(void **state) {
    char str[BUFFER_LEN] = "0.0.0.0	qwerty.com";
    char *expected = "qwerty.com";
    get_host(str);
    assert_string_equal(str, expected);
}

static void get_host_called_with_incorrect_ip_should_return_hostname(void **state) {
    char str[BUFFER_LEN] = "999.999.999.1234567890	qwerty.com";
    char *expected = "qwerty.com";
    get_host(str);
    assert_string_equal(str, expected);
}

static void get_host_called_with_integer_overflow_in_ip_should_return_hostname(void **state) {
    char str[BUFFER_LEN] = "999.999.999.9999999999999999	qwerty.com";
    char *expected = "qwerty.com";
    get_host(str);
    assert_string_equal(str, expected);
}

static void get_host_only_ip_address_should_return_empty_string(void **state) {
    char str[BUFFER_LEN] = "0.0.0.0	";
    char *expected = "";
    get_host(str);
    assert_string_equal(str, expected);
}

static void get_host_ip_contains_three_numbers_instead_of_four_should_return_empty_string(void **state) {
    char str[BUFFER_LEN] = "0.0.0	whatup.com";
    char *expected = "";
    get_host(str);
    assert_string_equal(str, expected);
}

static void get_host_host_greater_than_buffer_size_should_return_first_part_of_host(void **state) {
    char str[1024] = "0.0.0.0     thiswasatriumphIammakinganotehere:hugesuccess;it'shardtooverstatemystatisfaction'aperturesciencewedowhatwemustbecausewecanforthegoodofallofusexcepttheoneswhoaredead:butthereisnosensecryingovereverymistakewejustkeepontryingtillwerunoutofcakeandthesciencegetsdoneandyoumakeaneatgun.com";
    char *expected = "thiswasatriumphIammakinganotehere:hugesuccess;it'shardtooverstatemystatisfaction'aperturesciencewedowhatwemustbecausewecanforthegoodofallofusexcepttheoneswhoaredead:butthereisnosensecryingovereverymistakewejustkeepontryingtillwerunoutofcakeandthesciencege";
    get_host(str);
    assert_string_equal(str, expected);
}

static void get_host_no_space_between_ip_and_hostname_should_return_hostname(void **state) {
    char str[1024] = "0.0.0.0google.com";
    char *expected = "google.com";
    get_host(str);
    assert_string_equal(str, expected);
}

/* binary_search */
static void binary_search_sarray_with_single_item_should_match_matching_str(void **state) {
    sarray *arr = malloc(sizeof (sarray) + (START_SIZE_ARRAY * sizeof (char *)));
    arr->length = 1;
    arr->max = START_SIZE_ARRAY;
    arr->items[0] = strdup("google.com");
    int expected = 0;
    int actual = binary_search(arr, "google.com");
    free_sarray(arr);
    assert_int_equal(expected, actual);
}

static void binary_search_item_that_is_not_in_sarray_should_return_potential_index(void **state) {
    sarray *arr = malloc(sizeof (sarray) + (START_SIZE_ARRAY * sizeof (char *)));
    arr->length = 5;
    arr->max = START_SIZE_ARRAY;
    arr->items[0] = strdup("google.com");
    arr->items[1] = strdup("heineken.nl");
    arr->items[2] = strdup("indie.com");
    arr->items[3] = strdup("joker.gov");
    arr->items[4] = strdup("kleuters.nl");
    int expected = -6;
    int actual = binary_search(arr, "zzz.zzz");
    free_sarray(arr);
    assert_int_equal(expected, actual);
}

static void binary_search_item_that_is_smaller_than_items_in_sarray_should_return_minus_1(void **state) {
    sarray *arr = malloc(sizeof (sarray) + (START_SIZE_ARRAY * sizeof (char *)));
    arr->length = 5;
    arr->max = START_SIZE_ARRAY;
    arr->items[0] = strdup("google.com");
    arr->items[1] = strdup("heineken.nl");
    arr->items[2] = strdup("indie.com");
    arr->items[3] = strdup("joker.gov");
    arr->items[4] = strdup("kleuters.nl");
    int expected = -1;
    int actual = binary_search(arr, "abc.com");
    free_sarray(arr);
    assert_int_equal(expected, actual);
}

static void binary_search_sarray_with_single_item_should_not_match_nonmatching_str(void **state) {
    sarray *arr = malloc(sizeof (sarray) + (START_SIZE_ARRAY * sizeof (char *)));
    arr->length = 1;
    arr->max = START_SIZE_ARRAY;
    arr->items[0] = strdup("google.com");
    int expected = -1;
    int actual = binary_search(arr, "altavista.com");
    free_sarray(arr);
    assert_int_equal(expected, actual);
}

static void binary_search_sarray_with_multiple_items_should_match_value_in_the_middle(void **state) {
    sarray *arr = malloc(sizeof (sarray) + (START_SIZE_ARRAY * sizeof (char *)));
    arr->length = 5;
    arr->max = START_SIZE_ARRAY;
    arr->items[0] = strdup("av.com");
    arr->items[1] = strdup("google.com");
    arr->items[2] = strdup("peter.nl");
    arr->items[3] = strdup("qwerty.com");
    arr->items[4] = strdup("www.vk.com");
    int expected = 2;
    int actual = binary_search(arr, "peter.nl");
    free_sarray(arr);
    assert_int_equal(expected, actual);
}

static void binary_search_sarray_with_multiple_items_should_match_matching_value_at_the_end(void **state) {
    sarray *arr = malloc(sizeof (sarray) + (START_SIZE_ARRAY * sizeof (char *)));
    arr->length = 5;
    arr->max = START_SIZE_ARRAY;
    arr->items[0] = strdup("av.com");
    arr->items[1] = strdup("google.com");
    arr->items[2] = strdup("peter.nl");
    arr->items[3] = strdup("qwerty.com");
    arr->items[4] = strdup("www.vk.com");
    int expected = 4;
    int actual = binary_search(arr, "www.vk.com");
    free_sarray(arr);
    assert_int_equal(expected, actual);
}

static void binary_search_sarray_with_multiple_items_should_match_matching_value_at_the_start(void **state) {
    sarray *arr = malloc(sizeof (sarray) + (START_SIZE_ARRAY * sizeof (char *)));
    arr->length = 5;
    arr->max = START_SIZE_ARRAY;
    arr->items[0] = strdup("av.com");
    arr->items[1] = strdup("google.com");
    arr->items[2] = strdup("peter.nl");
    arr->items[3] = strdup("qwerty.com");
    arr->items[4] = strdup("www.vk.com");
    int expected = 0;
    int actual = binary_search(arr, "av.com");
    free_sarray(arr);
    assert_int_equal(expected, actual);
}

static void binary_search_sarray_with_multiple_items_should_return_projected_index_when_value_not_in_sarray(void **state) {
    sarray *arr = malloc(sizeof (sarray) + (START_SIZE_ARRAY * sizeof (char *)));
    arr->length = 5;
    arr->max = START_SIZE_ARRAY;
    arr->items[0] = strdup("av.com");
    arr->items[1] = strdup("google.com");
    arr->items[2] = strdup("peter.nl");
    arr->items[3] = strdup("qwerty.com");
    arr->items[4] = strdup("www.vk.com");
    int expected = -6;
    int actual = binary_search(arr, "zorro.com");
    printf("expected: %i\t\tactual: %i\n", expected, actual);
    free_sarray(arr);
    assert_int_equal(expected, actual);
}

/* sort_array */
static void sort_sarray_with_length_five_should_return_sorted_array(void **state) {
    sarray *arr1 = malloc(sizeof (sarray) + (START_SIZE_ARRAY * sizeof (char *)));
    arr1->length = 5;
    arr1->max = START_SIZE_ARRAY;
    arr1->items[0] = strdup("www.vk.com");
    arr1->items[1] = strdup("av.com");
    arr1->items[2] = strdup("peter.nl");
    arr1->items[3] = strdup("qwerty.com");
    arr1->items[4] = strdup("google.com");
    sort_sarray(arr1);
    // print_sarray(arr1);

    sarray *arr2 = malloc(sizeof (sarray) + (START_SIZE_ARRAY * sizeof (char *)));
    arr2->length = 5;
    arr2->max = START_SIZE_ARRAY;
    arr2->items[0] = strdup("av.com");
    arr2->items[1] = strdup("google.com");
    arr2->items[2] = strdup("peter.nl");
    arr2->items[3] = strdup("qwerty.com");
    arr2->items[4] = strdup("www.vk.com");
    // print_sarray(arr2);

    int expected = 0;
    int actual = compare_sarrays(arr1, arr2);

    free_sarray(arr1);
    free_sarray(arr2);

    assert_int_equal(expected, actual);
}

/* compare_str */
static void compare_str_should_return_0_when_both_strings_are_empty(void **state) {
    char *str1[] = {""};
    char *str2[] = {""};
    int expected = 0;
    int actual = compare_str(str1, str2);
    assert_int_equal(expected, actual);
}

static void compare_str_should_return_minus_one_when_first_string_comes_before_second(void **state) {
    char *str1[] = {"a"};
    char *str2[] = {"b"};
    int expected = -1;
    int actual = compare_str(str1, str2);
    assert_int_equal(expected, actual);
}

static void compare_str_should_return_0_when_strings_from_sarray_are_equal(void **state) {
    sarray *arr1 = (sarray *) malloc(sizeof (sarray) + START_SIZE_ARRAY * sizeof (char *));
    arr1->length = 1;
    arr1->max = START_SIZE_ARRAY;
    arr1->items[0] = strndup("whatever", BUFFER_LEN);
    sarray *arr2 = (sarray *) malloc(sizeof (sarray) + START_SIZE_ARRAY * sizeof (char *));
    arr2->length = 1;
    arr2->max = START_SIZE_ARRAY;
    arr2->items[0] = strndup("whatever", BUFFER_LEN);
    int expected = 0;
    int actual = compare_str(arr1->items, arr2->items);
    free_sarray(arr1);
    free_sarray(arr2);
    assert_int_equal(expected, actual);
}

static void compare_str_should_return_minus_1_when_first_string_from_sarray_comes_before_second_string(void **state) {
    sarray *arr1 = (sarray *) malloc(sizeof (sarray) + START_SIZE_ARRAY * sizeof (char *));
    arr1->length = 1;
    arr1->max = START_SIZE_ARRAY;
    arr1->items[0] = strndup("whatever", BUFFER_LEN);
    sarray *arr2 = (sarray *) malloc(sizeof (sarray) + START_SIZE_ARRAY * sizeof (char *));
    arr2->length = 1;
    arr2->max = START_SIZE_ARRAY;
    arr2->items[0] = strndup("zzzz", BUFFER_LEN);
    int expected = -1;
    int actual = compare_str(arr1->items, arr2->items);
    free_sarray(arr1);
    free_sarray(arr2);
    assert_int_equal(expected, actual);
}

static void compare_str_should_return_0_when_both_strings_are_equal(void **state) {
    char *str1[] = {"aqua"};
    char *str2[] = {"aqua"};
    int expected = 0;
    int actual = compare_str(str1, str2);
    assert_int_equal(expected, actual);
}

static void compare_str_should_return_1_when_first_string_comes_after_second_string(void **state) {
    char *str1[] = {"bear"};
    char *str2[] = {"aqua"};
    int expected = 1;
    int actual = compare_str(str1, str2);
    assert_int_equal(expected, actual);
}

static void compare_str_should_return_0_when_strings_are_the_same_for_the_first_BUFFER_LEN_chars(void **state) {
    char *str1[] = {"bearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbeaNOTTHIS"};
    char *str2[] = {"bearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbeaOTHERSTUFF"};
    int expected = 0;
    int actual = compare_str(str1, str2);
    assert_int_equal(expected, actual);
}

static void compare_str_should_return_1_when_strings_are_the_same_for_the_first_BUFFER_LEN_minus_1_chars(void **state) {
    char *str1[] = {"bearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbeYNOTTHIS"};
    char *str2[] = {"bearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbearbeXOTHERSTUFF"};
    int expected = 1;
    int actual = compare_str(str1, str2);
    assert_int_equal(expected, actual);
}

/* compare_sarrays */
static void compare_sarrays_should_return_1_when_length_is_not_equal(void **state) {
    sarray *arr1 = (sarray *) malloc(sizeof (sarray) + START_SIZE_ARRAY * sizeof (char *));
    arr1->length = 0;
    arr1->max = START_SIZE_ARRAY;
    sarray *arr2 = (sarray *) malloc(sizeof (sarray) + START_SIZE_ARRAY * sizeof (char *));
    arr2->length = 1;
    arr2->max = START_SIZE_ARRAY;
    arr2->items[0] = strndup("whatever", BUFFER_LEN);
    int expected = 1;
    int actual = compare_sarrays(arr1, arr2);

    free_sarray(arr1);
    free_sarray(arr2);

    assert_int_equal(expected, actual);
}

static void compare_sarrays_should_return_1_when_sarrays_items_are_not_equal(void **state) {
    sarray *arr1 = (sarray *) malloc(sizeof (sarray) + START_SIZE_ARRAY * sizeof (char *));
    arr1->length = 1;
    arr1->max = START_SIZE_ARRAY;
    arr1->items[0] = strndup("exactly", BUFFER_LEN);
    sarray *arr2 = (sarray *) malloc(sizeof (sarray) + START_SIZE_ARRAY * sizeof (char *));
    arr2->length = 1;
    arr2->max = START_SIZE_ARRAY;
    arr2->items[0] = strndup("whatever", BUFFER_LEN);
    int expected = 1;
    int actual = compare_sarrays(arr1, arr2);

    free_sarray(arr1);
    free_sarray(arr2);

    assert_int_equal(expected, actual);
}

static void compare_sarrays_should_return_0_when_sarrays_are_equal(void **state) {
    sarray *arr1 = (sarray *) malloc(sizeof (sarray) + START_SIZE_ARRAY * sizeof (char *));
    arr1->length = 2;
    arr1->max = START_SIZE_ARRAY;
    arr1->items[0] = strndup("exactly", BUFFER_LEN);
    arr1->items[1] = strndup("what", BUFFER_LEN);
    sarray *arr2 = (sarray *) malloc(sizeof (sarray) + START_SIZE_ARRAY * sizeof (char *));
    arr2->length = 2;
    arr2->max = START_SIZE_ARRAY;
    arr2->items[0] = strndup("exactly", BUFFER_LEN);
    arr2->items[1] = strndup("what", BUFFER_LEN);
    int expected = 0;
    int actual = compare_sarrays(arr1, arr2);

    free_sarray(arr1);
    free_sarray(arr2);

    assert_int_equal(expected, actual);
}

/* move_items_in_sarray */
static void move_items_in_sarray_index_in_middle_correctly_moves_items(void **state) {
    sarray *arr1 = malloc(sizeof (sarray) + START_SIZE_ARRAY * sizeof (char *));
    arr1->length = 2;
    arr1->max = START_SIZE_ARRAY;
    arr1->items[0] = strndup("exactly", BUFFER_LEN);
    arr1->items[1] = strndup("what", BUFFER_LEN);
    move_items_in_sarray(&arr1, 1);
    arr1->length++;
    // printf("`arr1->length is now %zu\n", arr1->length);
    arr1->items[1] = strndup("this", BUFFER_LEN);
    print_sarray(arr1);

    sarray *arr2 = malloc(sizeof (sarray) + START_SIZE_ARRAY * sizeof (char *));
    arr2->length = 3;
    arr2->max = START_SIZE_ARRAY;
    arr2->items[0] = strndup("exactly", BUFFER_LEN);
    arr2->items[1] = strndup("this", BUFFER_LEN);
    arr2->items[2] = strndup("what", BUFFER_LEN);
    print_sarray(arr2);

    int expected = 0;
    int result = compare_sarrays(arr1, arr2);
    // print_sarray(arr1);

    free_sarray(arr1);
    free_sarray(arr2);

    assert_int_equal(expected, result);
}

/* put_item_in_sorted_sarray */
static void put_item_in_sorted_sarray_puts_string_correctly_at_start(void **state) {
    sarray *arr1 = malloc(sizeof (sarray) + START_SIZE_ARRAY * sizeof (char *));
    arr1->length = 4;
    arr1->max = START_SIZE_ARRAY;
    arr1->items[0] = strndup("I", BUFFER_LEN);
    arr1->items[1] = strndup("exactly", BUFFER_LEN);
    arr1->items[2] = strndup("thought", BUFFER_LEN);
    arr1->items[3] = strndup("what", BUFFER_LEN);

    put_item_in_sorted_sarray(&arr1, "abc", 1);
    // print_sarray(arr1);

    sarray *arr2 = malloc(sizeof (sarray) + START_SIZE_ARRAY * sizeof (char *));
    arr2->length = 5;
    arr2->max = START_SIZE_ARRAY;
    arr2->items[0] = strndup("I", BUFFER_LEN);
    arr2->items[1] = strndup("abc", BUFFER_LEN);
    arr2->items[2] = strndup("exactly", BUFFER_LEN);
    arr2->items[3] = strndup("thought", BUFFER_LEN);
    arr2->items[4] = strndup("what", BUFFER_LEN);
    // print_sarray(arr2);

    int expected = 0;
    int result = compare_sarrays(arr1, arr2);

    free_sarray(arr1);
    free_sarray(arr2);

    assert_int_equal(expected, result);
}

static void merge_sarrays_two_single_item_sarrays_with_different_items_should_merge_to_a_two_item_sarray(void **state) {
    sarray *arr1 = malloc(sizeof (sarray) + START_SIZE_ARRAY * sizeof (char *));
    arr1->length = 1;
    arr1->max = START_SIZE_ARRAY;
    arr1->items[0] = strndup("exactly", BUFFER_LEN);

    sarray *arr2 = malloc(sizeof (sarray) + START_SIZE_ARRAY * sizeof (char *));
    arr2->length = 1;
    arr2->max = START_SIZE_ARRAY;
    arr2->items[0] = strndup("I", BUFFER_LEN);

    merge_sarrays(&arr1, &arr2, NULL);

    sarray *arr3 = malloc(sizeof (sarray) + START_SIZE_ARRAY * sizeof (char *));
    arr3->length = 2;
    arr3->max = START_SIZE_ARRAY;
    arr3->items[0] = strndup("I", BUFFER_LEN);
    arr3->items[1] = strndup("exactly", BUFFER_LEN);

    int expected = 0;
    int actual = compare_sarrays(arr1, arr3);

    free_sarray(arr1);
    free_sarray(arr2);
    free_sarray(arr3);

    assert_int_equal(expected, actual);
}

static void merge_sarrays_two_single_item_sarrays_with_same_items_should_result_in_single_item_sarray(void **state) {
    sarray *arr1 = malloc(sizeof (sarray) + START_SIZE_ARRAY * sizeof (char *));
    arr1->length = 1;
    arr1->max = START_SIZE_ARRAY;
    arr1->items[0] = strndup("exactly", BUFFER_LEN);

    sarray *arr2 = malloc(sizeof (sarray) + START_SIZE_ARRAY * sizeof (char *));
    arr2->length = 1;
    arr2->max = START_SIZE_ARRAY;
    arr2->items[0] = strndup("exactly", BUFFER_LEN);

    merge_sarrays(&arr1, &arr2, NULL);

    sarray *arr3 = malloc(sizeof (sarray) + START_SIZE_ARRAY * sizeof (char *));
    arr3->length = 1;
    arr3->max = START_SIZE_ARRAY;
    arr3->items[0] = strndup("exactly", BUFFER_LEN);

    int expected = 0;
    int actual = compare_sarrays(arr1, arr3);

    free_sarray(arr1);
    free_sarray(arr2);
    free_sarray(arr3);

    assert_int_equal(expected, actual);
}

static void write_sarray_to_file_should_write_sarray_with_single_item_to_file(void **state) {
    sarray *arr1 = malloc(sizeof (sarray) + START_SIZE_ARRAY * sizeof (char *));
    arr1->length = 1;
    arr1->max = START_SIZE_ARRAY;
    arr1->items[0] = strndup("exactly", BUFFER_LEN);

    FILE *f = open_file("output", "w");
    write_sarray_to_file(f, arr1, "0.0.0.0");
    close_file(f);
    f = open_file("output", "r");
    sarray *result = read_hosts(f);

    int expected = 0;
    int actual = compare_sarrays(arr1, result);

    close_file(f);

    free_sarray(arr1);
    free_sarray(result);

    assert_int_equal(expected, actual);
}

static void write_sarray_to_file_should_write_sarray_with_five_items_to_file(void **state) {
    sarray *arr1 = malloc(sizeof (sarray) + START_SIZE_ARRAY * sizeof (char *));
    arr1->length = 5;
    arr1->max = START_SIZE_ARRAY;
    arr1->items[0] = strndup("google.com", BUFFER_LEN);
    arr1->items[1] = strndup("altavista.com", BUFFER_LEN);
    arr1->items[2] = strndup("pvv.nl", BUFFER_LEN);
    arr1->items[3] = strndup("yanexocu.ru", BUFFER_LEN);
    arr1->items[4] = strndup("yanukovich.ua", BUFFER_LEN);

    FILE *f = open_file("output", "w");
    write_sarray_to_file(f, arr1, "0.0.0.0");
    close_file(f);
    f = open_file("output", "r");
    sarray *result = read_hosts(f);

    int expected = 0;
    int actual = compare_sarrays(arr1, result);

    close_file(f);

    free_sarray(arr1);
    free_sarray(result);

    assert_int_equal(expected, actual);
}

/* check_access */
static void check_access_existing_path_should_do_nothing(void **state) {
    const char *path = "noyuck.c";
    check_access(path, R_OK, 0);
}

/* append_suffix_to_str */
static void append_suffix_to_str_with_small_string_should_return_small_string_with_appended_suffix(void **state) {
    const char *str = "hello";
    const char *ext = ".ext";
    const char *expected = "hello.ext";
    char *actual = append_suffix_to_str(str, ext);
    assert_string_equal(expected, actual);
    free(actual);
}

static void append_suffix_to_str_with_300_char_string_should_return_truncated_str_with_full_extension(void **state) {
    const char *str = "hellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohello";
    const char *ext = ".ext";
    const char *expected = "hellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohello.ext";
    char *actual = append_suffix_to_str(str, ext);
    // printf("Actual str: %s\n", actual);
    assert_string_equal(expected, actual);
    free(actual);
}

static void append_suffix_to_str_with_300_plus_byte_extension_should_return_only_first_part_of_extension(void **state) {
    const char *str = "hello";
    const char *ext = ".ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext";
    const char *expected = ".ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.ext.e";
    char *actual = append_suffix_to_str(str, ext);
    // printf("Actual str: %s\n", actual);
    assert_string_equal(expected, actual);
    free(actual);
}

static void append_suffix_to_str_with_empty_str_and_ext_should_return_empty_string(void **state) {
    const char *str = "";
    const char *ext = "";
    const char *expected = "";
    char *actual = append_suffix_to_str(str, ext);
    // printf("Actual str: %s\n", actual);
    assert_string_equal(expected, actual);
    free(actual);
}

/* is_valid_prefix */
static void is_valid_prefix_should_return_1_when_prefix_is_valid(void **state) {
    const char *prefix = "0.0.0.0";  /* what about leading/trailing whitespace? */
    int expected = 1;
    int actual = is_valid_prefix(prefix);
    assert_int_equal(expected, actual);
}

static void is_valid_prefix_should_return_0_when_prefix_consists_of_four_digits_but_one_is_larger_than_255(void **state) {
    const char *prefix = "0.256.0.0";
    int expected = 0;
    int actual = is_valid_prefix(prefix);
    assert_int_equal(expected, actual);
}

static void is_valid_prefix_should_return_0_when_prefix_is_random_chars(void **state) {
    const char *prefix = "whatever";
    int expected = 0;
    int actual = is_valid_prefix(prefix);
    assert_int_equal(expected, actual);
}

static void is_valid_prefix_should_return_0_when_prefix_is_empty_string(void **state) {
    const char *prefix = "";
    int expected = 0;
    int actual = is_valid_prefix(prefix);
    assert_int_equal(expected, actual);
}

/* str_matches */
static void str_matches_should_return_0_when_empty_str_is_passed(void **state) {
    const char *regex = "^(https?:\\/\\/)?([0-9a-z\\.-]+)\\.([a-z\\.]{2,6})([\\/a-zA-Z0-9_ \\.?&=-]*)\\/?$";
    regex_t comp;
    init_regex(regex, &comp);
    const char *url = "";
    int expected = 0;
    int actual = str_matches(&comp, url);
    regfree(&comp);
    assert_int_equal(expected, actual);
}

static void str_matches_should_return_1_when_passed_url_is_valid(void **state) {
    const char *regex = "^(https?:\\/\\/)?([0-9a-z\\.-]+)\\.([a-z\\.]{2,6})([\\/a-zA-Z0-9_ \\.?&=-]*)\\/?$";
    regex_t comp;
    init_regex(regex, &comp);
    LargestIntegralType results[4];
    const char *url[] = {"https://adaway.org/hosts",
                         "http://pgl.yoyo.org/adservers/serverlist.php?hostformat=hosts&showintro=0&mimetype=plaintext",
                         "hosts-file.net/ad_servers.asp",
                         "winhelp2002.mvps.org/hosts.txt"};
    for (int i = 0; i < 4; i++) {
        results[i] = str_matches(&comp, url[i]);
    }
    regfree(&comp);
    assert_not_in_set(0, results, 4);
}

static void str_matches_should_return_0_when_passed_url_is_invalid(void **state) {
    const char *regex = "^(https?:\\/\\/)?([0-9a-z\\.-]+)\\.([a-z\\.]{2,6})([\\/a-zA-Z0-9_ \\.?&=-]*)\\/?$";
    regex_t comp;
    init_regex(regex, &comp);
    LargestIntegralType results[4];
    const char *url[] = {"http:///adaway.org/hosts",
                         "htttp://pgl.yoyo.orgorgo/adservers/serverlist.php?hostformat=hosts&showintro=0&mimetype=plaintext",
                         "hosts-file",
                         "winhelp2002.mvps.org/hosts.txt$"};
    for (int i = 0; i < 4; i++) {
        results[i] = str_matches(&comp, url[i]);
    }
    regfree(&comp);
    assert_not_in_set(1, results, 4);
}

/* starts_with */
static void starts_with_should_return_1_when_pre_is_empty_string(void **state) {
    const char *str = "hello";
    const char *pre = "";
    int expected = 1;
    int actual = starts_with(str, pre);
    assert_int_equal(expected, actual);
}

static void starts_with_should_return_1_when_both_params_are_empty_strings(void **state) {
    const char *str = "";
    const char *pre = "";
    int expected = 1;
    int actual = starts_with(str, pre);
    assert_int_equal(expected, actual);
}

static void starts_with_should_return_0_when_str_is_empty_and_pre_not(void **state) {
    const char *str = "";
    const char *pre = "whatever";
    int expected = 0;
    int actual = starts_with(str, pre);
    assert_int_equal(expected, actual);
}

static void starts_with_should_return_1_when_pre_matches_str(void **state) {
    const char *str = "hello";
    const char *pre = "hello";
    int expected = 1;
    int actual = starts_with(str, pre);
    assert_int_equal(expected, actual);
}

static void starts_with_should_return_0_when_pre_is_longer_than_str(void **state) {
    const char *str = "hello";
    const char *pre = "hello buddy";
    int expected = 0;
    int actual = starts_with(str, pre);
    assert_int_equal(expected, actual);
}

static void starts_with_should_return_0_when_both_params_are_equally_long_but_differ(void **state) {
    const char *str = "hello frien";
    const char *pre = "hello buddy";
    int expected = 0;
    int actual = starts_with(str, pre);
    assert_int_equal(expected, actual);
}

static void starts_with_should_return_0_when_pre_does_not_match_str(void **state) {
    const char *str = "hello";
    const char *pre = "hai";
    int expected = 0;
    int actual = starts_with(str, pre);
    assert_int_equal(expected, actual);
}

/* move_char_array */
static void move_char_array_should_move_array_one_position_to_the_left(void **state) {
    char str[BUFFER_LEN] = "hello";
    int places = 1;
    char *expected = "ello";
    move_char_array(str, places);
    assert_string_equal(expected, str);
}

static void move_char_array_should_move_array_three_positions_to_the_left(void **state) {
    char str[BUFFER_LEN] = "hello";
    int places = 3;
    char *expected = "lo";
    move_char_array(str, places);
    assert_string_equal(expected, str);
}

static void move_char_array_should_change_str_to_empty_string_when_places_is_larger_than_strlen(void **state) {
    char str[BUFFER_LEN] = "hello";
    int places = 10;
    char *expected = "";
    move_char_array(str, places);
    assert_string_equal(expected, str);
}

static void move_char_array_should_change_str_to_empty_str_when_str_is_empty(void **state) {
    char str[BUFFER_LEN] = "";
    int places = 1;
    char *expected = "";
    move_char_array(str, places);
    assert_string_equal(expected, str);
}

static void move_char_array_should_return_empty_str_when_places_is_minus_1(void **state) {
    char str[BUFFER_LEN] = "whatever";
    int places = -1;
    char *expected = "";
    move_char_array(str, places);
    assert_string_equal(expected, str);
}

/* get_part_of_url */
static void get_part_of_url_should_change_valid_url_to_domain(void **state) {
    char url[BUFFER_LEN] = "www.domain.com/whatever";
    char *expected = "www.domain.com";
    get_part_of_url(url, START);
    assert_string_equal(expected, url);
}

static void get_part_of_url_should_return_empty_str_when_input_is_empty_str(void **state) {
    char url[BUFFER_LEN] = "";
    char *expected = "";
    get_part_of_url(url, START);
    assert_string_equal(expected, url);
}

static void get_part_of_url_should_change_valid_url_with_prefix_http_to_domain(void **state) {
    char url[BUFFER_LEN] = "http://www.domain.com/whatever";
    char *expected = "www.domain.com";
    get_part_of_url(url, START);
    assert_string_equal(expected, url);
}

static void get_part_of_url_should_handle_really_long_str_correctly(void **state) {
    char url[BUFFER_LEN] = "http://www.domain.com/whatever/whatever/whatever/whatever/whatever/whatever/whatever/whatever/whatever/whatever/whatever/whatever/whatever/whatever/whatever/whatever/whatever/whatever/whatever/whatever/whatever/whatever/whatever/whatever/whatever/whateve";
    char *expected = "www.domain.com";
    get_part_of_url(url, START);
    assert_string_equal(expected, url);
}

static void get_part_of_url_should_change_url_to_part_after_first_dash_when_startOrEnd_is_END(void **state) {
    char url[BUFFER_LEN] = "http://www.domain.com/whatever";
    char *expected = "/whatever";
    get_part_of_url(url, END);
    assert_string_equal(expected, url);
}

static void get_part_of_url_should_change_url_to_correct_path_with_various_options(void **state) {
    char url[BUFFER_LEN] = "http://www.domain.com/whatever?one=true&yes=no";
    char *expected = "/whatever?one=true&yes=no";
    get_part_of_url(url, END);
    assert_string_equal(expected, url);
}

/* put_header */
static void put_header_should_put_correct_header_in_target_file(void **state) {
    const char *filename = "headertest";
    FILE *f = open_file(filename, "w+");
    /* check if file contents are the same as expected */
    const char *expected =
        "# Hosts file generated by NoYuck on Thu Mar 24 20:08:16 2016\n\n"
        "127.0.0.1\tlocalhost.localdomain\tlocalhost\n"
        "::1\t\tlocalhost.localdomain\tlocalhost\n"
        "\n# Start of fetched hosts\n\n";
    int chars_put = put_header(f);
    close_file(f);
    if (unlink(filename) == -1) {
        error("Could not remove file");
    }
    assert_int_equal(strlen(expected), chars_put);
}

int main(void) {
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(null_test_should_succeed),
        cmocka_unit_test(get_host_with_normal_input_should_return_host),
        cmocka_unit_test(get_host_with_empty_string_should_return_empty_string),
        cmocka_unit_test(get_host_called_with_incorrect_ip_should_return_hostname),
        cmocka_unit_test(get_host_called_with_integer_overflow_in_ip_should_return_hostname),
        cmocka_unit_test(get_host_only_ip_address_should_return_empty_string),
        cmocka_unit_test(get_host_ip_contains_three_numbers_instead_of_four_should_return_empty_string),
        cmocka_unit_test(get_host_host_greater_than_buffer_size_should_return_first_part_of_host),
        cmocka_unit_test(get_host_no_space_between_ip_and_hostname_should_return_hostname),
        cmocka_unit_test(binary_search_sarray_with_single_item_should_match_matching_str),
        cmocka_unit_test(binary_search_sarray_with_single_item_should_not_match_nonmatching_str),
        cmocka_unit_test(binary_search_sarray_with_multiple_items_should_match_value_in_the_middle),
        cmocka_unit_test(binary_search_sarray_with_multiple_items_should_match_matching_value_at_the_end),
        cmocka_unit_test(binary_search_sarray_with_multiple_items_should_match_matching_value_at_the_start),
        cmocka_unit_test(binary_search_sarray_with_multiple_items_should_return_projected_index_when_value_not_in_sarray),
        cmocka_unit_test(sort_sarray_with_length_five_should_return_sorted_array),
        cmocka_unit_test(compare_str_should_return_minus_one_when_first_string_comes_before_second),
        cmocka_unit_test(compare_str_should_return_0_when_both_strings_are_empty),
        cmocka_unit_test(compare_str_should_return_0_when_both_strings_are_equal),
        cmocka_unit_test(compare_str_should_return_1_when_first_string_comes_after_second_string),
        cmocka_unit_test(compare_str_should_return_0_when_strings_are_the_same_for_the_first_BUFFER_LEN_chars),
        cmocka_unit_test(compare_str_should_return_1_when_strings_are_the_same_for_the_first_BUFFER_LEN_minus_1_chars),
        cmocka_unit_test(compare_sarrays_should_return_1_when_length_is_not_equal),
        cmocka_unit_test(compare_str_should_return_0_when_strings_from_sarray_are_equal),
        cmocka_unit_test(compare_str_should_return_minus_1_when_first_string_from_sarray_comes_before_second_string),
        cmocka_unit_test(compare_sarrays_should_return_1_when_sarrays_items_are_not_equal),
        cmocka_unit_test(compare_sarrays_should_return_0_when_sarrays_are_equal),
        cmocka_unit_test(move_items_in_sarray_index_in_middle_correctly_moves_items),
        cmocka_unit_test(put_item_in_sorted_sarray_puts_string_correctly_at_start),
        cmocka_unit_test(binary_search_item_that_is_not_in_sarray_should_return_potential_index),
        cmocka_unit_test(binary_search_item_that_is_smaller_than_items_in_sarray_should_return_minus_1),
        cmocka_unit_test(merge_sarrays_two_single_item_sarrays_with_different_items_should_merge_to_a_two_item_sarray),
        cmocka_unit_test(merge_sarrays_two_single_item_sarrays_with_same_items_should_result_in_single_item_sarray),
        cmocka_unit_test(write_sarray_to_file_should_write_sarray_with_single_item_to_file),
        cmocka_unit_test(write_sarray_to_file_should_write_sarray_with_five_items_to_file),
        cmocka_unit_test(check_access_existing_path_should_do_nothing),
        cmocka_unit_test(append_suffix_to_str_with_small_string_should_return_small_string_with_appended_suffix),
        cmocka_unit_test(append_suffix_to_str_with_300_char_string_should_return_truncated_str_with_full_extension),
        cmocka_unit_test(append_suffix_to_str_with_300_plus_byte_extension_should_return_only_first_part_of_extension),
        cmocka_unit_test(append_suffix_to_str_with_empty_str_and_ext_should_return_empty_string),
        cmocka_unit_test(is_valid_prefix_should_return_1_when_prefix_is_valid),
        cmocka_unit_test(is_valid_prefix_should_return_0_when_prefix_consists_of_four_digits_but_one_is_larger_than_255),
        cmocka_unit_test(is_valid_prefix_should_return_0_when_prefix_is_random_chars),
        cmocka_unit_test(is_valid_prefix_should_return_0_when_prefix_is_empty_string),
        cmocka_unit_test(str_matches_should_return_1_when_passed_url_is_valid),
        cmocka_unit_test(str_matches_should_return_0_when_empty_str_is_passed),
        cmocka_unit_test(str_matches_should_return_0_when_passed_url_is_invalid),
        cmocka_unit_test(starts_with_should_return_1_when_pre_is_empty_string),
        cmocka_unit_test(starts_with_should_return_1_when_pre_matches_str),
        cmocka_unit_test(starts_with_should_return_0_when_pre_is_longer_than_str),
        cmocka_unit_test(starts_with_should_return_1_when_both_params_are_empty_strings),
        cmocka_unit_test(starts_with_should_return_0_when_str_is_empty_and_pre_not),
        cmocka_unit_test(starts_with_should_return_0_when_pre_does_not_match_str),
        cmocka_unit_test(starts_with_should_return_0_when_both_params_are_equally_long_but_differ),
        cmocka_unit_test(get_part_of_url_should_change_valid_url_to_domain),
        cmocka_unit_test(get_part_of_url_should_change_valid_url_with_prefix_http_to_domain),
        cmocka_unit_test(move_char_array_should_move_array_one_position_to_the_left),
        cmocka_unit_test(move_char_array_should_change_str_to_empty_string_when_places_is_larger_than_strlen),
        cmocka_unit_test(move_char_array_should_change_str_to_empty_str_when_str_is_empty),
        cmocka_unit_test(move_char_array_should_return_empty_str_when_places_is_minus_1),
        cmocka_unit_test(move_char_array_should_move_array_three_positions_to_the_left),
        cmocka_unit_test(get_part_of_url_should_return_empty_str_when_input_is_empty_str),
        cmocka_unit_test(get_part_of_url_should_handle_really_long_str_correctly),
        cmocka_unit_test(get_part_of_url_should_change_url_to_part_after_first_dash_when_startOrEnd_is_END),
        cmocka_unit_test(get_part_of_url_should_change_url_to_correct_path_with_various_options),
        cmocka_unit_test(put_header_should_put_correct_header_in_target_file)
    };

    return cmocka_run_group_tests(tests, NULL, NULL);
}
