#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>
#include <regex.h>
#include "include/util.h"

void usage() {
    fprintf(stderr,
            "noyuck: usage: noyuck --current HOSTS1 --new HOSTS2 --prefix PREFIX\n");
    exit(EXIT_FAILURE);
}

void update_from_file(const char *path_current,
                      const char *path_new,
                      const char *path_output,
                      const char *prefix,
                      int in_place,
                      const char *path_whitelist)
{
    sarray *whitelist_hosts = create_sarray_from_path(path_whitelist, 0, NULL);
    print_sarray(whitelist_hosts);
    sarray *current = NULL;
    sarray *new = NULL;

    /* add non-overlapping elements from new to current */
    if (whitelist_hosts == NULL) {
        current = create_sarray_from_path(path_current, 1, NULL);
        new = create_sarray_from_path(path_new, 1, NULL);
        merge_sarrays(&current, &new, NULL);
    } else {
        current = create_sarray_from_path(path_current, 1, &whitelist_hosts);
        new = create_sarray_from_path(path_new, 1, &whitelist_hosts);
        merge_sarrays(&current, &new, &whitelist_hosts);
    }

    if (in_place) {
        path_output = path_current;
        /* backup existing hosts file */
        backup_file(path_current, ".bak");
    }

    /* write the sarray to the output file */
    FILE *file_output = open_file(path_output, "w");
    put_header(file_output);
    write_sarray_to_file(file_output, current, prefix);
    close_file(file_output);

    free_sarray(current);
    free_sarray(new);
    if (whitelist_hosts != NULL) {
        free_sarray(whitelist_hosts);
    }
}

void update_from_urls(const char *path_current,
                      const char *path_conf_file,
                      const char *path_output,
                      const char *prefix,
                      int in_place,
                      const char *path_whitelist)
{
    sarray *whitelist_hosts = create_sarray_from_path(path_whitelist, 0, NULL);
    sarray *current_hosts = NULL;

    if (whitelist_hosts == NULL) {
        current_hosts = create_sarray_from_path(path_current, 1, NULL);
    } else {
        current_hosts = create_sarray_from_path(path_current, 1, &whitelist_hosts);
    }

    check_access(path_conf_file, R_OK, 1);
    FILE *conf = open_file(path_conf_file, "r");

    /* read URLs file and check for valid URLs */
    const char *url_regex = "^(https?:\\/\\/)?([0-9a-z\\.-]+)\\.([a-z\\.]{2,6})([\\/a-zA-Z0-9_ \\.?&=-]*)\\/?$";
    regex_t comp;
    init_regex(url_regex, &comp);

    /* open file to temporarily store the host */
    const char *path_temp_hosts_file = ".temp_hosts_file";
    check_access(path_temp_hosts_file, W_OK, 0);
    FILE *temp;

    char next_url[BUFFER_LEN];
    while (fgets(next_url, BUFFER_LEN, conf) != NULL) {
        next_url[strcspn(next_url, "\n\r")] = '\0';

        char domain[BUFFER_LEN];
        strncpy(domain, next_url, BUFFER_LEN);
        get_part_of_url(domain, START);
        char path[BUFFER_LEN];
        strncpy(path, next_url, BUFFER_LEN);
        get_part_of_url(path, END);

        if (str_matches(&comp, next_url)) {
            /* we should really use a pipe or fifo here */
            temp = open_file(path_temp_hosts_file, "w+");
            /* make a connection to the URLs and read the files */
            int socket = get_connection(domain, "80");

            /* talk to server! */
            char get_request[BUFFER_LEN];
            snprintf(get_request, BUFFER_LEN, "GET %s HTTP/1.1\r\n", path);
            char host_request[BUFFER_LEN];
            snprintf(host_request, BUFFER_LEN, "Host: %s\r\n\r\n", domain);
            say_to_server(socket, get_request);
            say_to_server(socket, host_request);

            read_into_file(socket, temp);
            close(socket);

            rewind(temp);

            sarray *new_hosts = NULL;
            /* merge current hosts and remote hosts files */
            if (whitelist_hosts != NULL) {
                new_hosts = create_sarray_from_file(temp, &whitelist_hosts);
                merge_sarrays(&current_hosts, &new_hosts, &whitelist_hosts);
            } else {
                new_hosts = create_sarray_from_file(temp, NULL);
                merge_sarrays(&current_hosts, &new_hosts, NULL);
            }
            free_sarray(new_hosts);
            close_file(temp);
        }
    }

    if (in_place) {
        path_output = path_current;
        /* backup existing hosts file */
        backup_file(path_current, ".bak");
    }

    check_access(path_output, W_OK, 0);
    FILE *out = open_file(path_output, "w");

    put_header(out);
    write_sarray_to_file(out, current_hosts, prefix);

    free_sarray(current_hosts);
    if (whitelist_hosts != NULL) {
        free_sarray(whitelist_hosts);
    }

    /* free the compiled regex */
    regfree(&comp);

    close_file(conf);
    close_file(out);
}

int main(int argc, char *argv[]) {
    /* return value from getopt_long */
    int opt;

    /* variables that will be set by command-line args */
    char *path_current = NULL;
    char *path_new = NULL;
    char *path_output = "merged_hosts";
    char *path_conf_file = NULL;
    char *path_whitelist = NULL;
    char *prefix = "0.0.0.0";
    int in_place = 0;

    /* array of option structs laying out long and short options */
    static struct option long_options[] = {
        {"current", required_argument, 0, 'c'},
        {"file", required_argument, 0, 'f'},
        {"new", required_argument, 0, 'n'},
        {"prefix", required_argument, 0, 'p'},
        {"inplace", no_argument, 0, 'i'},
        {"whitelist", no_argument, 0, 'w'},
        {0, 0, 0, 0}
    };

    int long_index = 0;
    while ((opt = getopt_long(argc, argv, "c:f:n:p:iw:", long_options, &long_index)) != -1) {
        switch (opt) {
            case 'c':
                path_current = optarg;
                break;
            case 'f':
                path_conf_file = optarg;
                break;
            case 'n':
                path_new = optarg;
                break;
            case 'p':
                prefix = optarg;
                break;
            case 'i':
                in_place = 1;
                break;
            case 'w':
                path_whitelist = optarg;
                break;
            default:
                usage();
                break;  /* superfluous: usage() exits the program */
        }
    }

    if (!is_valid_prefix(prefix)) {
        fprintf(stderr, "Not a valid prefix: %s\n", prefix);
        exit(EXIT_FAILURE);
    }

    /* TODO: path_current _could_ default to /etc/hosts */
    if (path_current == NULL) {
        usage();
    } else if (path_conf_file != NULL) {
        update_from_urls(path_current, path_conf_file, path_output, prefix, in_place, path_whitelist);
    } else if (path_new != NULL) {
        update_from_file(path_current, path_new, path_output, prefix, in_place, path_whitelist);
    }

    return 0;
}
